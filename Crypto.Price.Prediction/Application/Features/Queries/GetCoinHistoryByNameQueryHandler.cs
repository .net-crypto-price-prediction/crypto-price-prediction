﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinHistoryByNameQueryHandler : IRequestHandler<GetCoinHistoryByNameQuery, IList<CoinHistory>>
    {
        private readonly ICoinHistoryRepository repository;
        public GetCoinHistoryByNameQueryHandler(ICoinHistoryRepository repository)
        {
            this.repository = repository;
        }

        async Task<IList<CoinHistory>> IRequestHandler<GetCoinHistoryByNameQuery, IList<CoinHistory>>.Handle(GetCoinHistoryByNameQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetByName(request.Name);
        }
    }
}