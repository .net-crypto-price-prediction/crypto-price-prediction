﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetAllInterestsQueryHandler : IRequestHandler<GetAllInterestsQuery, IEnumerable<Interest>>
    {
        private readonly IInterestRepository repository;

        public GetAllInterestsQueryHandler(IInterestRepository repository)
        {
            this.repository = repository;
        }
        public async Task<IEnumerable<Interest>> Handle(GetAllInterestsQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetAllAsync();
        }
    }
}
