﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetInterestsByUserIdQuery : IRequest<IEnumerable<Interest>>
    {
        public Guid Id { get; set; }
    }
}
