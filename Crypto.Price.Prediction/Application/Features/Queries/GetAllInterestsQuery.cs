﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetAllInterestsQuery : IRequest<IEnumerable<Interest>>
    {
    }
}
