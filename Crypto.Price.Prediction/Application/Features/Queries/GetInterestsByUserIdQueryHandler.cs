﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetInterestsByUserIdQueryHandler : IRequestHandler<GetInterestsByUserIdQuery, IEnumerable<Interest>>
    {
        private readonly IInterestRepository repository;

        public GetInterestsByUserIdQueryHandler(IInterestRepository repository)
        {
            this.repository = repository;
        }

        public async Task<IEnumerable<Interest>> Handle(GetInterestsByUserIdQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetByUserIdAsync(request.Id);
        }
    }
}
