﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinHistoryQueryHandler : IRequestHandler<GetCoinHistoryQuery, IEnumerable<CoinHistory>>
    {
        private readonly ICoinHistoryRepository repository;

        public GetCoinHistoryQueryHandler(ICoinHistoryRepository repository)
        {
            this.repository = repository;
        }
        public async Task<IEnumerable<CoinHistory>> Handle(GetCoinHistoryQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetAllAsync();
        }
    }
}
