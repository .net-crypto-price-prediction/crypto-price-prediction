﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinHistoryQuery : IRequest<IEnumerable<CoinHistory>>
    {
    }
}
