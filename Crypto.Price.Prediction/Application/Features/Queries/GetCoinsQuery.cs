﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinsQuery : IRequest<IEnumerable<Coin>>
    {
    }
}
