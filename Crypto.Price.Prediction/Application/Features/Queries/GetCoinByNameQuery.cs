﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinByNameQuery : IRequest<Coin>
    {
        public string Name { get; set; } = null!;
    }
}
