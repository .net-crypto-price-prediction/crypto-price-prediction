﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinByNameQueryHandler : IRequestHandler<GetCoinByNameQuery, Coin>
    {
        private readonly ICoinRepository repository;
        public GetCoinByNameQueryHandler(ICoinRepository repository)
        {
            this.repository = repository;
        }
        public async Task<Coin> Handle(GetCoinByNameQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetByNameAsync(request.Name);
        }
    }
}
