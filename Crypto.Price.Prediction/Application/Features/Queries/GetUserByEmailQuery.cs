﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetUserByEmailQuery : IRequest<User>
    {
        public string Email { get; set; } = null!;
    }
}
