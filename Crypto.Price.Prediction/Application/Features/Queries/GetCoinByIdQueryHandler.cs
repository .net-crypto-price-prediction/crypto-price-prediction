﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinByIdQueryHandler : IRequestHandler<GetCoinByIdQuery, Coin>
    {
        private readonly ICoinRepository repository;
        public GetCoinByIdQueryHandler(ICoinRepository repository)
        {
            this.repository = repository;
        }
        public async Task<Coin> Handle(GetCoinByIdQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetByIdAsync(request.Id);
        }
    }
}
