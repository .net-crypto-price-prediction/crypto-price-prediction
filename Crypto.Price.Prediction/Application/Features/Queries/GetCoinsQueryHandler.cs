﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinsQueryHandler : IRequestHandler<GetCoinsQuery, IEnumerable<Coin>>
    {
        private readonly ICoinRepository repository;

        public GetCoinsQueryHandler(ICoinRepository repository)
        {
            this.repository = repository;
        }
        public async Task<IEnumerable<Coin>> Handle(GetCoinsQuery request, CancellationToken cancellationToken)
        {
            return await repository.GetAllAsync();
        }
    }
}
