﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinHistoryByNameQuery : IRequest<IList<CoinHistory>>
    {
        public string Name { get; set; } = null!;
    }
}
