﻿using Domain.Entities;
using MediatR;

namespace Application.Features.Queries
{
    public class GetCoinByIdQuery : IRequest<Coin>
    {
        public Guid Id { get; set; }
    }
}
