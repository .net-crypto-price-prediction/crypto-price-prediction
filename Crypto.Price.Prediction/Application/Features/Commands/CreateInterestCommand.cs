﻿using MediatR;

namespace Application.Features.Commands
{
    public class CreateInterestCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }
        public Guid CoinId { get; set; }
    }
}
