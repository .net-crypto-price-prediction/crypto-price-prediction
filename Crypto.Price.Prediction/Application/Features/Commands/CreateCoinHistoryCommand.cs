﻿using MediatR;
using Domain.Entities;
namespace Application.Features.Commands
{
    public class CreateCoinHistoryCommand : IRequest<Guid>
    {
        public Guid CoinId { get; set; }
        public string Symbol { get; set; } = null!;
        public string Name { get; set; } = null!;
        public double Current_Price { get; set; }
        public double Market_Cap { get; set; }
        public double Total_Volume { get; set; }
        public DateTime? Date { get; set; }
    }
}
