﻿using MediatR;

namespace Application.Features.Commands
{
    public class UpdateUserCommand : IRequest<Guid>
    {
        public Guid? Id { get; set; }
        public string Email { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string Token { get; set; } = null!;
        public string PreferredCurrency { get; set; } = null!;

    }
}
