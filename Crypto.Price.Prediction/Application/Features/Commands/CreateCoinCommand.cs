﻿using MediatR;

namespace Application.Features.Commands
{
    public class CreateCoinCommand : IRequest<Guid>
    {
        public string Symbol { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string Image { get; set; } = null!;
        public double Ath { get; set; }
        public double Ath_Change_Percentage { get; set; }
        public DateTime Ath_Date { get; set; }
        public double Atl { get; set; }
        public double Atl_Change_Percentage { get; set; }
        public DateTime Atl_Date { get; set; }
    }
}
