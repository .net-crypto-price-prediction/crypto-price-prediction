﻿using Application.Interfaces;
using MediatR;

namespace Application.Features.Commands
{
    public class DeleteUserByEmailCommandHandler: IRequestHandler<DeleteUserByEmailCommand, Guid>
    {
        private readonly IUserRepository repository;

        public DeleteUserByEmailCommandHandler(IUserRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Guid> Handle(DeleteUserByEmailCommand request, CancellationToken cancellationToken)
        {
            CheckParameters(request);
            var user = repository.GetByEmailAsync(request.Email).Result;
            await repository.DeleteAsync(user);
            return user.Id;
        }

        private static void CheckParameters(DeleteUserByEmailCommand request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request), "Null command");
            }
        }
    }
}
