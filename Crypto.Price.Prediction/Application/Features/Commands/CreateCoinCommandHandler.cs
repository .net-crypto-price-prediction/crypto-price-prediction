﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Commands
{
    public class CreateCoinCommandHandler : IRequestHandler<CreateCoinCommand, Guid>
    {
        private readonly ICoinRepository repository;

        public CreateCoinCommandHandler(ICoinRepository repository)
        {
            this.repository = repository;
        }
        public async Task<Guid> Handle(CreateCoinCommand request, CancellationToken cancellationToken)
        {
            var coin = new Coin
            {
                Symbol = request.Symbol,
                Name = request.Name,
                Image = request.Image,
                Ath = request.Ath,
                Ath_Change_Percentage = request.Ath_Change_Percentage,
                Ath_Date = request.Ath_Date,
                Atl = request.Atl,
                Atl_Change_Percentage = request.Atl_Change_Percentage,
                Atl_Date = request.Atl_Date
            };

            await repository.AddAsync(coin);
            return coin.Id;
        }
    }
}
