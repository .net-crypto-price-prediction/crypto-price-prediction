﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Commands
{
    public class CreateInterestCommandHandler : IRequestHandler<CreateInterestCommand, Guid>
    {
        private readonly IInterestRepository repository;

        public CreateInterestCommandHandler(IInterestRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Guid> Handle(CreateInterestCommand request, CancellationToken cancellationToken)
        {
            var interest = new Interest
            {
                UserId = request.UserId,
                CoinId = request.CoinId
            };

            await repository.AddAsync(interest);
            return interest.Id;
        }
    }
}
