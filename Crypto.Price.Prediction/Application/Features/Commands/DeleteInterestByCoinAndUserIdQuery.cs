﻿using MediatR;

namespace Application.Features.Commands
{
    public class DeleteInterestByCoinAndUserIdQuery : IRequest<Guid>
    {
        public Guid CoinId { get; set; }
        public Guid UserId { get; set; }
    }
}
