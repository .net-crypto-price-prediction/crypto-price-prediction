﻿using Application.Interfaces;
using MediatR;

namespace Application.Features.Commands
{
    public class DeleteInterestByCoinAndUserIdQueryHandler : IRequestHandler<DeleteInterestByCoinAndUserIdQuery, Guid>
    {
        private readonly IInterestRepository repository;

        public DeleteInterestByCoinAndUserIdQueryHandler(IInterestRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Guid> Handle(DeleteInterestByCoinAndUserIdQuery request, CancellationToken cancellationToken)
        {
            CheckParameters(request);
            var interest = await repository.GetByCoinAndUserIdAsync(request.CoinId, request.UserId);
            await repository.DeleteAsync(interest);
            return interest.Id;
        }

        private static void CheckParameters(DeleteInterestByCoinAndUserIdQuery request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request), "Null query");
            }
        }
    }
}
