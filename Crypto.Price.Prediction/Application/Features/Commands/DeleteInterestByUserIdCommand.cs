﻿using MediatR;

namespace Application.Features.Commands
{
    public class DeleteInterestByUserIdCommand : IRequest<IEnumerable<Guid>>
    {
        public Guid UserId { get; set; }
    }
}
