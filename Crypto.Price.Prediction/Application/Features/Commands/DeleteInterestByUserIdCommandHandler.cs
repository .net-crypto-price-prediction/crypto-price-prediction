﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Commands
{
    public class DeleteInterestByUserIdCommandHandler : IRequestHandler<DeleteInterestByUserIdCommand, IEnumerable<Guid>>
    {
        private readonly IInterestRepository repository;

        public DeleteInterestByUserIdCommandHandler(IInterestRepository repository)
        {
            this.repository = repository;
        }

        public async Task<IEnumerable<Guid>> Handle(DeleteInterestByUserIdCommand request, CancellationToken cancellationToken)
        {
            CheckParameters(request);
            IList<Interest> interests = await repository.GetByUserIdAsync(request.UserId);
            List<Guid> interestsIds = new List<Guid>();
            interests.ToList().ForEach(async interest => 
            {
                interestsIds.Add(interest.Id);
                await repository.DeleteAsync(interest);
            });

            return interestsIds;
        }

        private static void CheckParameters(DeleteInterestByUserIdCommand request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request), "Null command");
            }
        }
    }
}
