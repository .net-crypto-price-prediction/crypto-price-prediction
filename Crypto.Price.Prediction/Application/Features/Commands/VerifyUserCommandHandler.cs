﻿using Application.Interfaces;
using MediatR;

namespace Application.Features.Commands
{
    public class VerifyUserCommandHandler : IRequestHandler<VerifyUserCommand, Guid>
    {
        private readonly IUserRepository repository;

        public VerifyUserCommandHandler(IUserRepository repository)
        {
            this.repository = repository;
        }

        public async Task<Guid> Handle(VerifyUserCommand request, CancellationToken cancellationToken)
        {
            var user = repository.GetByIdAsync(request.Id).Result;

            if (user == null)
            {
                return Guid.Empty;
            }

            user.IsVerified = true;

            await repository.UpdateAsync(user);
            return user.Id;
        }
    }
}
