﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Commands
{
    public class CreateCoinHistoryCommandHandler : IRequestHandler<CreateCoinHistoryCommand, Guid>
    {
        private readonly ICoinHistoryRepository repository;

        public CreateCoinHistoryCommandHandler(ICoinHistoryRepository repository)
        {
            this.repository = repository;
        }
        public async Task<Guid> Handle(CreateCoinHistoryCommand request, CancellationToken cancellationToken)
        {
            var coinHistory = new CoinHistory
            {
                CoinId = request.CoinId,
                Symbol = request.Symbol,
                Name = request.Name,
                Current_Price = request.Current_Price,
                Market_Cap = request.Market_Cap,
                Total_Volume = request.Total_Volume,
                Date = request.Date
    };
            await repository.AddAsync(coinHistory);
            return coinHistory.Id;
        }
    }
}
