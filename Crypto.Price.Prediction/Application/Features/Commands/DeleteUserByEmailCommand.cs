﻿using MediatR;

namespace Application.Features.Commands
{
    public class DeleteUserByEmailCommand : IRequest<Guid>
    {
        public string Email { get; set; } = null!;
    }
}
