﻿using Application.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Features.Commands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Guid>
    {
        private readonly IUserRepository repository;

        public UpdateUserCommandHandler(IUserRepository repository)
        {
            this.repository = repository;
        }
        public async Task<Guid> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = repository.GetByIdAsync(request.Id == null ? Guid.Empty : (Guid)request.Id).Result;
            CheckParameters(user);

            user.FirstName = string.IsNullOrEmpty(request.FirstName) || string.IsNullOrWhiteSpace(request.FirstName) ? user.FirstName : request.FirstName;
            user.LastName = string.IsNullOrEmpty(request.LastName) || string.IsNullOrWhiteSpace(request.LastName) ? user.LastName : request.LastName;
            user.Password = string.IsNullOrEmpty(request.Password) || string.IsNullOrWhiteSpace(request.Password) ? user.Password : request.Password;
            user.Token = string.IsNullOrEmpty(request.Token) || string.IsNullOrWhiteSpace(request.Token) ? user.Token : request.Token;
            user.PreferredCurrency = string.IsNullOrEmpty(request.PreferredCurrency) || string.IsNullOrWhiteSpace(request.PreferredCurrency) ? user.PreferredCurrency : request.PreferredCurrency;

            await repository.UpdateAsync(user);

            return user.Id;
        }

        private static void CheckParameters(User user)
        {
            if (user == null || user.Id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(user), "User doesn't exist");
            }
        }
    }
}
