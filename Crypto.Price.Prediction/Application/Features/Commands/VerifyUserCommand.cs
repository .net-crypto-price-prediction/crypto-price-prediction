﻿using MediatR;

namespace Application.Features.Commands
{
    public class VerifyUserCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
    }
}
