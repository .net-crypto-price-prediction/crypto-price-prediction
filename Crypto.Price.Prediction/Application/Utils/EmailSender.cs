﻿using SendGrid;
using SendGrid.Helpers.Mail;

namespace Application.Utils
{
    public static class EmailSender
    {
        private static Secrets secrets = new Secrets();
        private static string FromEmail = "crypto.price.prediction.net@gmail.com";
        private static string FromName = "Crypto Price Prediction";
        private static string EmailVerificationTemplateId = "d-f562b21c1d464de78a6eb2a8e14227f6";

        public async static Task SendMail(string toMail, string toName, string redirectLink)
        {
            var client = new SendGridClient(secrets.SendGridApiKey);
            var message = new SendGridMessage();
            message.SetFrom(FromEmail, FromName);
            message.AddTo(toMail, toName);
            message.SetTemplateId(EmailVerificationTemplateId);
            message.SetTemplateData(new {
                name = toName,
                link = redirectLink
            });

            await client.SendEmailAsync(message);
        }
    }
}
