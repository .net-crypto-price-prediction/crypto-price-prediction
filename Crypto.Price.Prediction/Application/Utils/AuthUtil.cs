﻿using Domain.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Application.Utils
{
    public class AuthUtil
    {
        private readonly IConfiguration _configuration;

        public AuthUtil(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Authentication, user.Email)
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Secret").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(3),
                signingCredentials: creds);

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public static string DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenDecode = handler.ReadJwtToken(token);
            if (tokenDecode != null)
            {
                return tokenDecode.Claims.First().Value;
            }
            else
            {
                return string.Empty;
            }
        }

        public static bool VerifyPasswords(string toBeVerified, string verifyAgainst)
        {
            return toBeVerified.Equals(verifyAgainst);
        }

        public static Pair<bool, string> isValidToken(string token)
        {
            Pair<bool, string> pair = new Pair<bool, string>(false, string.Empty);
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                pair.Second = "Token is not set";
                return pair;
            }
            string userId = AuthUtil.DecodeToken(token);
            if (userId == null)
            {
                pair.Second = "Invalid token";
                return pair;
            }
            pair.First = true;
            pair.Second = userId;
            return pair;
        }
    }
}
