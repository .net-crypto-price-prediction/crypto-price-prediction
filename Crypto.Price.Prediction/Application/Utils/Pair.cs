﻿namespace Application.Utils
{
    public class Pair<T, E>
    {
        public T First { get; set; }
        public E Second { get; set; }

        public Pair(T first, E second)
        {
            First = first;
            Second = second;
        }

    }
}
