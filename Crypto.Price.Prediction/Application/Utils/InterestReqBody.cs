﻿namespace Application.Utils
{
    public class InterestReqBody
    {
        public string? UserEmail { get; set; }
        public string? CoinName { get; set; }
    }
}
