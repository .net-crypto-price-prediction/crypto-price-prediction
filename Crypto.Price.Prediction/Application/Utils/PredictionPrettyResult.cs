﻿namespace Application.Utils
{
    public class PredictionPrettyResult
    {
        public float Price { get; set; }
        public string DateText { get; set; } = null!;
    }
}
