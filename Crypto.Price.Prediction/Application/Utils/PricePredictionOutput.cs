﻿namespace Application.Utils
{
    public class PricePredictionOutput
    {
        public float[] PredictedPrice { get; set; } = null!;
    }
}
