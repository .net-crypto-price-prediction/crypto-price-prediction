﻿namespace Application.Utils
{
    public class CoinHistoryForPrediction
    {
        public float Current_Price { get; set; }
        public float Market_Cap { get; set; }
        public float Total_Volume { get; set; }
    }
}
