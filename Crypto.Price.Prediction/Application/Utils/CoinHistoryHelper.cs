﻿using Newtonsoft.Json.Linq;

namespace Application.Utils
{
    public static class CoinHistoryHelper
    {
        public static double GetCurrentPrice(string str)
        {
            JObject json = JObject.Parse(str);
            if (json["market_data"] == null || json["market_data"]["current_price"] == null)
                return 0;
            double response = (json["market_data"]["current_price"]["usd"] != null ? (double)json["market_data"]["current_price"]["usd"] : 0);
            return response;
        }
        public static double GetMarketCap(string str)
        {
            JObject json = JObject.Parse(str);
            if (json["market_data"] == null || json["market_data"]["market_cap"] == null)
                return 0;
            double response = (json["market_data"]["market_cap"]["usd"] != null ? (double)json["market_data"]["market_cap"]["usd"] : 0);
            return response;
        }
        public static double GetTotalVolume(string str)
        {
            JObject json = JObject.Parse(str);
            if (json["market_data"] == null || json["market_data"]["total_volume"] == null)
                return 0;
            double response = (json["market_data"]["total_volume"]["usd"] != null ? (double)json["market_data"]["total_volume"]["usd"] : 0);
            return response;
        }
        public static string GetName(string str)
        {
            JObject json = JObject.Parse(str);
            return ((string)json["name"]);
        }
        public static string GetSymbol(string str)
        {
            JObject json = JObject.Parse(str);
            return ((string)json["symbol"]);
        }
    }
}
