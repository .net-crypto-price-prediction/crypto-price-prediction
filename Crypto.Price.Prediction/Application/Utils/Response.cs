﻿namespace Application.Utils
{
    public class Response
    {
        public string message { get; set; }

        public Response(string message)
        {
            this.message = message;
        }
    }

    public class UnsuccessfulResponse : Response
    {
        public UnsuccessfulResponse(string message, int statusCode) : base(message)
        {
            this.statusCode = statusCode;
        }

        public int statusCode { get; set; }
    }

    public class AuthenticationResponse
    {
        public string token { get; set; }

        public AuthenticationResponse(string token)
        {
            this.token = token;
        }
    }
}
