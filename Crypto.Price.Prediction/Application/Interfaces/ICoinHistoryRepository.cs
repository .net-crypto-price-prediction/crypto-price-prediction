﻿using Domain.Entities;

namespace Application.Interfaces
{
    public interface ICoinHistoryRepository : IRepository<CoinHistory>
    {
        Task<IList<CoinHistory>> GetByName(string name);
    }
}
