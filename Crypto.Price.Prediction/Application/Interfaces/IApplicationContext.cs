﻿using Microsoft.EntityFrameworkCore;

namespace Application.Interfaces
{
    public interface IApplicationContext
    {
        DbSet<Domain.Entities.User> Users { get; set; }

        DbSet<Domain.Entities.Coin> Coins { get; set; }

        DbSet<Domain.Entities.CoinHistory> CoinsHistory { get; set; }

        DbSet<Domain.Entities.Interest> Interests { get; set; }

        Task<int> SaveChangesAsync();
    }
}
