﻿using Domain.Entities;

namespace Application.Interfaces
{
    public interface ICoinRepository : IRepository<Coin>
    {
        Task<Coin> GetByNameAsync(string name);
    }
}
