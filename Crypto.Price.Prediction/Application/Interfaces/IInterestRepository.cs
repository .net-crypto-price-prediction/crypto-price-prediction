﻿using Domain.Entities;

namespace Application.Interfaces
{
    public interface IInterestRepository : IRepository<Interest>
    {
        Task<IList<Interest>> GetByUserIdAsync(Guid userId);

        Task<Interest> GetByCoinAndUserIdAsync(Guid coinId, Guid userId);
    }
}
