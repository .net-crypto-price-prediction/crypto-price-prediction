﻿using Application.Features.Commands;
using Application.Utils;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using WebAPI.Controllers.v1;
using Xunit;

namespace Tests
{
    public class AuthenticationControllerTests
    {
        private readonly AuthenticationController controller;
        private readonly IMediator mediator;
        public AuthenticationControllerTests(IConfiguration configuration)
        {
            mediator = A.Fake<IMediator>();
            controller = new AuthenticationController(mediator, configuration);
        }

        /*[Fact]
        public async void Given_AuthenticationController_When_VerifyUserIsCalled_Then_ShouldVerifyUser()
        {
            await controller.VerifyUser(new Guid("630405ab-9710-4061-b403-53b6f02ea709"));
            A.CallTo(() => mediator.Send(A<VerifyUserCommand>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_AuthenticationController_When_LoginIsCalledSuccessfully_Then_ShouldReturnA_AuthToken()
        {
            await controller.Login(new UserRequest
            {
                Email = "jdoe@gmail.com",
                Password = "7627cb9027e713e301e83a8f13057055"
            });
            //ToDo
        }*/

    }
}
