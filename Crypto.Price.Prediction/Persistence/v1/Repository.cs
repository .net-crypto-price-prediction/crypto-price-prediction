﻿using Application.Interfaces;
using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.v1
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly CryptoPricePredictionContext context;

        public Repository(CryptoPricePredictionContext context)
        {
            this.context = context;
        }
        public async Task<TEntity> AddAsync(TEntity entity)
        {
            CheckParameters(entity, nameof(AddAsync));
            await context.AddAsync(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> DeleteAsync(TEntity entity)
        {
            CheckParameters(entity, nameof(DeleteAsync));
            context.Remove(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            CheckParameters(id, nameof(GetByIdAsync));
            return await context.FindAsync<TEntity>(id);
        }

        public async Task<TEntity> GetByEmailAsync(string email)
        {
            CheckParameters(email, nameof(GetByEmailAsync));
            return await context.FindAsync<TEntity>(email);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            CheckParameters(entity, nameof(UpdateAsync));
            context.Update(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        private static void CheckParameters(TEntity entity, string methodName)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity), $"{methodName} entity must not be null");
            }
        }

        private static void CheckParameters(Guid id, string methodName)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException($"{methodName} id must not be empty");
            }
        }

        private static void CheckParameters(string email, string methodName)
        {
            if (email == string.Empty)
            {
                throw new ArgumentException($"{methodName} email must not be empty");
            }
        }
    }
}
