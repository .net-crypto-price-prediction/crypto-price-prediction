﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.v1
{
    public class CoinHistoryRepository : Repository<CoinHistory>, ICoinHistoryRepository
    {
        private readonly CryptoPricePredictionContext _context;
        public CoinHistoryRepository(CryptoPricePredictionContext context) : base(context)
        {
            _context = context;
        }
        public async Task<IList<CoinHistory>> GetByName(string name)
        {
            return await _context.CoinsHistory.Where(c => c.Name == name).ToListAsync();
        }
    }
}
