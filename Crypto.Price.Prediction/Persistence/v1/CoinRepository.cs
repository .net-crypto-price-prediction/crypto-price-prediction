﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.v1
{
    public class CoinRepository : Repository<Coin>, ICoinRepository
    {
        private readonly CryptoPricePredictionContext _context;
        public CoinRepository(CryptoPricePredictionContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Coin> GetByNameAsync(string name)
        {
            return await _context.Coins.FirstOrDefaultAsync(c => c.Name == name);
        }
    }
}
