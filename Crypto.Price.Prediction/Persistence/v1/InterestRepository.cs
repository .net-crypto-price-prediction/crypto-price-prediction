﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.v1
{
    public class InterestRepository : Repository<Interest>, IInterestRepository
    {
        private readonly CryptoPricePredictionContext _context;

        public InterestRepository(CryptoPricePredictionContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Interest> GetByCoinAndUserIdAsync(Guid coinId, Guid userId)
        {
            List<Interest> interests = await _context.Interests.Where(i => i.UserId == userId && i.CoinId == coinId).ToListAsync();
            CheckParameters(interests);
            return interests[0];
        }

        public async Task<IList<Interest>> GetByUserIdAsync(Guid userId)
        {
            return await _context.Interests.Where(i => i.UserId == userId).ToListAsync();
        }

        private static void CheckParameters(List<Interest> interests)
        {
            if (interests == null || interests.Count == 0)
            {
                throw new ArgumentNullException(nameof(interests), "Interest not found");
            }
        }
    }
}
