﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;

namespace Persistence.v1
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly CryptoPricePredictionContext _context;
        public UserRepository(CryptoPricePredictionContext context) : base(context)
        {
            _context = context;
        }

        public new async Task<User> GetByEmailAsync(string email)
        {
           return await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
        }
    }
}
