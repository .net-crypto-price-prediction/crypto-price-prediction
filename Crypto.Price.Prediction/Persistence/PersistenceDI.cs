﻿using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using Microsoft.Extensions.DependencyInjection;
using Application.Interfaces;
using Persistence.v1;

namespace Persistence
{
    public static class PersistenceDI
    {
        public static void AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CryptoPricePredictionContext>(options => options.UseSqlite("Data Source=CryptoPrice.db"));

            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ICoinRepository, CoinRepository>();
            services.AddTransient<ICoinHistoryRepository, CoinHistoryRepository>();
            services.AddTransient<IInterestRepository, InterestRepository>();
        }
    }
}
