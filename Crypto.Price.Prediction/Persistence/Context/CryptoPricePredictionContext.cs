﻿using Microsoft.EntityFrameworkCore;
using Application.Interfaces;
using Domain.Entities;

namespace Persistence.Context
{
    public class CryptoPricePredictionContext : DbContext, IApplicationContext
    {
        /*public CryptoPricePredictionContext()
        {

        }*/
        public CryptoPricePredictionContext(DbContextOptions<CryptoPricePredictionContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Coin> Coins { get ; set ; } = null!;
        public DbSet<CoinHistory> CoinsHistory { get; set; } = null!;
        public DbSet<Interest> Interests { get; set; } = null!;

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=CryptoPrice.db");
        }*/
    }
}
