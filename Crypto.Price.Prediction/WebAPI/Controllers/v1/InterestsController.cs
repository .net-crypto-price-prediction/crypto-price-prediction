﻿using Application.Features.Commands;
using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class InterestsController : BaseController
    {
        public InterestsController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await mediator.Send(new GetAllInterestsQuery()));
        }
        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpDelete("allForUser")]
        public async Task<IActionResult> DeleteAllForUser([FromHeader] string token)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Token is not set", 401)));
            }
            string userEmail = AuthUtil.DecodeToken(token);
            if (string.IsNullOrEmpty(userEmail))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            var userToCheck = await mediator.Send(new GetUserByEmailQuery
            {
                Email = userEmail
            });
            if (userToCheck == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            return Ok(await mediator.Send(new DeleteInterestByUserIdCommand()
            {
                UserId = userToCheck.Id
            }));
        }
        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpDelete("byName")]
        public async Task<IActionResult> DeleteByCoinName([FromHeader] string token, [FromQuery] string coinName)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Token is not set", 401)));
            }
            string userEmail = AuthUtil.DecodeToken(token);
            if (string.IsNullOrEmpty(userEmail))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            var userToCheck = await mediator.Send(new GetUserByEmailQuery
            {
                Email = userEmail
            });
            if (userToCheck == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            var coin = await mediator.Send(new GetCoinByNameQuery()
            {
                Name = coinName
            });
            if (coin == null)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found", 404)));
            }

            try
            {
                return Ok(await mediator.Send(new DeleteInterestByCoinAndUserIdQuery()
                {
                    CoinId = coin.Id,
                    UserId = userToCheck.Id
                }));
            }
            catch (Exception ex)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse(ex.Message, 404)));
            }

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost]
        public async Task<IActionResult> AddInterest([FromHeader] string token, [FromBody] InterestReqBody interestReqBody)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token) || interestReqBody == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null or empty fields are not allowed", 400)));
            }
            string userEmail = AuthUtil.DecodeToken(token);
            if (string.IsNullOrEmpty(userEmail))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            var userToCheck = await mediator.Send(new GetUserByEmailQuery
            {
                Email = userEmail
            });
            if (userToCheck == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            if (string.IsNullOrEmpty(interestReqBody.UserEmail) || string.IsNullOrEmpty(interestReqBody.CoinName))
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid data", 400)));
            }
            User user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = interestReqBody.UserEmail
            });
            
            Coin coin = await mediator.Send(new GetCoinByNameQuery()
            {
                Name = interestReqBody.CoinName
            });

            if (user == null || coin == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Resources not found", 400)));
            }

            CreateInterestCommand command = new CreateInterestCommand
            {
                UserId = user.Id,
                CoinId = coin.Id
            };

            await mediator.Send(command);

            return CreatedAtAction(nameof(AddInterest), System.Text.Json.JsonSerializer.Serialize(new Response("Resources created successfully")));
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("forUser")]
        public async Task<IActionResult> GetByUser([FromHeader] string token)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Token is not set", 401)));
            }
            Console.WriteLine(token);
            string userEmail = AuthUtil.DecodeToken(token);
            Console.WriteLine(userEmail);
            if (userEmail == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }
            
            User user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = userEmail
            });

            if (user == default)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("User not found", 404)));
            }

            IEnumerable<Interest> interests = await mediator.Send(new GetInterestsByUserIdQuery()
            {
                Id = user.Id
            });

            List<Coin> coins = new List<Coin>();
            interests.ToList().ForEach(async interest =>
            {
                Coin coin = await mediator.Send(new GetCoinByIdQuery()
                {
                    Id = interest.CoinId
                });
                coins.Add(coin);
            });

            return Ok(coins);
        }
    }
}
