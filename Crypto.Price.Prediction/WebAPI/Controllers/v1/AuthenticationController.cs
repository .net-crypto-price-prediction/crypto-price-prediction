﻿using Application.Features.Commands;
using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class AuthenticationController : BaseController
    {
        private const string _verifyPath = "https://localhost:7157/api/1.0/Authentication/verify?id=";
        private readonly AuthUtil authUtil;
        public static string VerifyPath => _verifyPath;

        public AuthenticationController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {
            authUtil = new AuthUtil(configuration);
        }

        [HttpPost("Register")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create([FromBody] CreateUserCommand command)
        {
            if (command == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null command.", 400)));
            }

            if (command.Email != null)
            {
                bool isValid = RegexUtil.IsValidEmail(command.Email);
                if (!isValid)
                {
                    return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Not a valid email", 400)));
                }
            } 
            else
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Email can't be null.", 400)));
            }
            User user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = command.Email
            });
            if (user != default && user.Email != null)
            {
                return Conflict(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Email is taken", 409)));
            }
            await mediator.Send(command);

            user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = command.Email
            });
            EmailSender.SendMail(command.Email, command.FirstName, VerifyPath + user.Id).Wait();

            return CreatedAtAction(nameof(Create), System.Text.Json.JsonSerializer.Serialize(new Response("User created successfully!")));

        }

        [HttpPost("Login")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Login([FromBody] UserRequest request)
        {
            User user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = request.Email
            });
            
            if (user == null || !AuthUtil.VerifyPasswords(request.Password, user.Password))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid credentials", 401)));
            }
            if (!user.IsVerified)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Please verify your email!", 401)));
            }

            string token = authUtil.CreateToken(user);
            user.Token = token;

            await mediator.Send(new UpdateUserCommand
            {
                Id = user.Id,
                Email = user.Email,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = token,
                PreferredCurrency = user.PreferredCurrency,
            });
            return Ok(System.Text.Json.JsonSerializer.Serialize(new AuthenticationResponse(token)));
        }

        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpGet, Route("verify")]
        public async Task<IActionResult> VerifyUser([FromQuery] Guid id)
        {
            var verifyUserCommand = new VerifyUserCommand
            {
                Id = id
            };
            await mediator.Send(verifyUserCommand);
            return NoContent();
        }
    }
}
