﻿using Application.Features.Commands;
using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CoinsController : BaseController
    {
        private const string _populateWithCoins = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";

        public CoinsController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await mediator.Send(new GetCoinsQuery()));
        }
        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("byId")]
        public async Task<IActionResult> GetById([FromQuery] GetCoinByIdQuery query)
        {
            if (query.Id == Guid.Empty)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found because id is empty.", 404)));
            }
            var coin = await mediator.Send(query);
            if (coin == default)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found.", 404)));
            }
            else
            {
                return Ok(coin);
            }
        }
        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("byName")]
        public async Task<IActionResult> GetByName([FromQuery] GetCoinByNameQuery query)
        {
            if (string.IsNullOrEmpty(query.Name) || string.IsNullOrWhiteSpace(query.Name))
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found because name is empty.", 404)));
            }
            var coin = await mediator.Send(query);
            if (coin == default)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found.", 404)));
            }
            else
            {
                return Ok(coin);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost]
        public async Task<IActionResult> Populate([FromQuery] string secret)
        {
            if (secret == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null value for secret", 400)));
            }
            if (secret != _configuration.GetSection("AppSettings:Secret").Value)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Not a valid secret", 401)));
            }

            List<Coin> coinsList = new List<Coin>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(_populateWithCoins))
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        IEnumerable<Coin> coins = JsonConvert.DeserializeObject<IEnumerable<Coin>>(apiResponse);
                        if (coins == null)
                        {
                            return Problem(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null value for secret", 400)));
                        }
                        coinsList = coins.ToList();
                    }
                }

                if (coinsList != null && coinsList.Count > 0)
                {
                    coinsList.ForEach(async coinItem => await mediator.Send(createCoin(coinItem)));
                }
            }
            return CreatedAtAction(nameof(Populate), System.Text.Json.JsonSerializer.Serialize(new Response("Resources created successfully")));
        }


        private static CreateCoinCommand createCoin(Coin coin)
        {
            return new CreateCoinCommand()
            {
                Symbol = string.IsNullOrEmpty(coin.Symbol) ? "" : coin.Symbol,
                Name = string.IsNullOrEmpty(coin.Name) ? "" : coin.Name,
                Image = string.IsNullOrEmpty(coin.Image) ? "" : coin.Image,
                Ath = coin.Ath == null ? 0 : (double)coin.Ath,
                Ath_Change_Percentage = coin.Ath_Change_Percentage == null ? 0 : (double)coin.Ath_Change_Percentage,
                Ath_Date = coin.Ath_Date == null ? default(DateTime) : (DateTime)coin.Ath_Date,
                Atl = coin.Ath == null ? 0 : (double)coin.Ath,
                Atl_Change_Percentage = coin.Atl_Change_Percentage == null ? 0 : (double)coin.Atl_Change_Percentage,
                Atl_Date = coin.Atl_Date == null ? default(DateTime) : (DateTime)coin.Atl_Date
            };
        }

    }
}
