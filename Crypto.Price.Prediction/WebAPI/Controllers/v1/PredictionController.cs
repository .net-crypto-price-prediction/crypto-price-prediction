﻿using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ML;
using Microsoft.ML.Transforms.TimeSeries;

namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class PredictionController : BaseController
    {
        private readonly MLContext mlContext;
        public PredictionController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {
            mlContext = new MLContext(seed: 0);
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("train")]
        public async Task<IActionResult> Train([FromQuery] string secret)
        {
            if (secret == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null value for secret", 400)));
            }
            if (secret != _configuration.GetSection("AppSettings:Secret").Value)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Not a valid secret", 401)));
            }

            IEnumerable<Coin> coinsList = await mediator.Send(new GetCoinsQuery());

            coinsList.ToList().ForEach(coin =>
            {
                IEnumerable<CoinHistory> coinHistory = mediator.Send(new GetCoinHistoryByNameQuery()
                {
                    Name = string.IsNullOrEmpty(coin.Name) ? "" : coin.Name
                }).Result;

                List<CoinHistoryForPrediction> forPredictions = new List<CoinHistoryForPrediction>();
                coinHistory.ToList().ForEach(coinHistory =>
                {
                    forPredictions.Add(new CoinHistoryForPrediction()
                    {
                        Current_Price = coinHistory.Current_Price == null ? 0 : (float)coinHistory.Current_Price,
                        Market_Cap = coinHistory.Market_Cap == null ? 0 : (float)coinHistory.Market_Cap,
                        Total_Volume = coinHistory.Total_Volume == null ? 0 : (float)coinHistory.Total_Volume
                    });
                });

                IDataView trainingData = mlContext.Data.LoadFromEnumerable(forPredictions);

                var trainerr = mlContext.Forecasting.ForecastBySsa(
                    outputColumnName: nameof(PricePrediction.PredictedPrice),
                    inputColumnName: nameof(CoinHistoryForPrediction.Current_Price),
                    windowSize: 12,
                    seriesLength: 13,
                    trainSize: 12 * 2 + 1,
                    horizon: 12,
                    confidenceLevel: 0.95f,
                    confidenceLowerBoundColumn: "Features",
                    confidenceUpperBoundColumn: "Features");

                var trainedModel = trainerr.Fit(trainingData);

                Console.WriteLine("saving..");
                mlContext.Model.Save(trainedModel, trainingData.Schema, @"models\" + coin.Name + "_Trained_Model.zip");
            });

            return Ok(System.Text.Json.JsonSerializer.Serialize(new Response("Trained model")));
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet("predict")]
        public async Task<IActionResult> Predict([FromHeader] string token, [FromQuery] string coinName)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Token is not set", 401)));
            }
            string userEmail = AuthUtil.DecodeToken(token);
            if (string.IsNullOrEmpty(userEmail))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            var userToCheck = await mediator.Send(new GetUserByEmailQuery
            {
                Email = userEmail
            });
            if (userToCheck == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }

            string filePath = @"models\" + coinName + "_Trained_Model.zip";
            if (!System.IO.File.Exists(filePath))
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Unable to predict prices for given coin name", 404)));
            }

            ITransformer trainedModel = mlContext.Model.Load(filePath, out var modelSchema);

            var predEngine = trainedModel.CreateTimeSeriesEngine<CoinHistoryForPrediction, PricePredictionOutput>(mlContext);

            var resultprediction = predEngine.Predict(new CoinHistoryForPrediction());

            List<PredictionPrettyResult> prettyResults = new List<PredictionPrettyResult>();
            DateTime from = DateTime.Now.AddMonths(1);
            resultprediction.PredictedPrice.ToList().ForEach(p =>
            {
                string _dateText = from.ToString("MMMM") + "-" + from.ToString("yyyy");
                prettyResults.Add(new PredictionPrettyResult()
                {
                    Price = p,
                    DateText = _dateText
                });
                from = from.AddMonths(1);
            });
            return Ok(prettyResults);
        }
    }
}
