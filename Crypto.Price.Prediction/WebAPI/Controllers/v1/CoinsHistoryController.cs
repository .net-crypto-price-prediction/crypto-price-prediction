﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Application.Features.Commands;
using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using Newtonsoft.Json;
namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CoinsHistoryController : BaseController
    {
        private const string _populateWithCoins = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";
        private const string _populateWithCoinsHistory = "https://api.coingecko.com/api/v3/coins/";

        public CoinsHistoryController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {

        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await mediator.Send(new GetCoinHistoryQuery()));
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("byName")]
        public async Task<IActionResult> GetByName([FromQuery] GetCoinHistoryByNameQuery query)
        {
            if (string.IsNullOrEmpty(query.Name) || string.IsNullOrWhiteSpace(query.Name))
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Coin not found because name is empty.", 404)));
            }
            
            IList<CoinHistory> coins = await mediator.Send(query);
            return Ok(coins);
        }
        private async Task<List<CoinHistory>> GetAll()
        {
            List<CoinHelper> coinsList = new List<CoinHelper>();
            using (var httpClient = new HttpClient())
            {
                using (var response = httpClient.GetAsync(_populateWithCoins).Result)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        IEnumerable<CoinHelper> coins = JsonConvert.DeserializeObject<IEnumerable<CoinHelper>>(apiResponse);
                        
                        coinsList = coins.ToList();
                    }
                }
            }

            IEnumerable<Coin> coinsListEnumarable = await mediator.Send(new GetCoinsQuery());

            List<Coin> coinsList1 = coinsListEnumarable.ToList();
            List<CoinHistory> coinHistory = new List<CoinHistory>();
            int i = 0;
            int times = 0;
            if (coinsList != null && coinsList.Count > 0)
            {
                coinsList.ForEach(coinItem =>
                {
                    Console.WriteLine(coinItem.id);
                    string id = string.IsNullOrEmpty(coinItem.id) ? "" : coinItem.id;
                    Thread.Sleep(10000);
                    DateTime from = new DateTime(2019, 12, 31);
                    DateTime thru = new DateTime(2021, 12, 31);
                    Pair<List<CoinHistory>, int> pair = CreateCoinsHistoryList(from, thru, id, i, times, coinsList1);
                    Console.WriteLine("exit");
                    times = pair.Second;
                    coinHistory =  coinHistory.Concat(pair.First).ToList();
                });
                i++;
            }
            return coinHistory;
        }

        private static Pair<List<CoinHistory>, int> CreateCoinsHistoryList(DateTime from, DateTime thru, string id, int i, int times, List<Coin> coinsList)
        {
            List<CoinHistory> coinHistory = new List<CoinHistory>();

            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(28))
            {
                string date = day.ToString("dd-MM-yyyy");
                string _url = _populateWithCoinsHistory + id + "/history?date=" + date;

                CoinHistory coin = new CoinHistory();

                using (var httpClient = new HttpClient())
                {
                    using (var response = httpClient.GetAsync(_url).Result)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            string apiResponse = response.Content.ReadAsStringAsync().Result;
                            coin.CoinId = coinsList[i].Id;
                            coin.Name = CoinHistoryHelper.GetName(apiResponse);
                            coin.Symbol = CoinHistoryHelper.GetSymbol(apiResponse);
                            coin.Current_Price = CoinHistoryHelper.GetCurrentPrice(apiResponse);
                            coin.Market_Cap = CoinHistoryHelper.GetMarketCap(apiResponse);
                            coin.Total_Volume = CoinHistoryHelper.GetTotalVolume(apiResponse);
                            coin.Date = day.Date;

                            if (coin.Current_Price != 0 || coin.Market_Cap != 0 || coin.Total_Volume != 0)
                            {
                                coinHistory.Add(coin);
                                Console.WriteLine(coin.ToString());
                            }
                        }
                    }
                }
                times++;
                if (times == 50)
                {
                    Thread.Sleep(60000);
                    times = 0;
                }
            }
            return new Pair<List<CoinHistory>, int>(coinHistory, times);
        }


        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpPost]
        public async Task<IActionResult> Populate([FromQuery] string secret)
        {
            if (secret == null)
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Null value for secret", 400)));
            }
            if (secret != _configuration.GetSection("AppSettings:Secret").Value)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Not a valid secret", 401)));
            }
            List<CoinHistory> coinHistories = await Task.Run(async () => await GetAll());

            coinHistories.ForEach(async coin => await mediator.Send(createCoinHistory(coin)));

            return CreatedAtAction(nameof(Populate), System.Text.Json.JsonSerializer.Serialize(new Response("Resources created successfully")));
        }

        private static CreateCoinHistoryCommand createCoinHistory(CoinHistory coinHistory)
        {
            return new CreateCoinHistoryCommand()
            {
                CoinId = coinHistory.CoinId,
                Name = string.IsNullOrEmpty(coinHistory.Name) ? "" : coinHistory.Name,
                Symbol = string.IsNullOrEmpty(coinHistory.Symbol) ? "" : coinHistory.Symbol,
                Current_Price = coinHistory.Current_Price == null ? 0 : (double)coinHistory.Current_Price,
                Market_Cap = coinHistory.Market_Cap == null ? 0 : (double)coinHistory.Market_Cap,
                Total_Volume = coinHistory.Total_Volume == null ? 0 : (double)coinHistory.Total_Volume,
                Date = coinHistory.Date == null ? default(DateTime) : (DateTime)coinHistory.Date
            };
        }


    }
}
