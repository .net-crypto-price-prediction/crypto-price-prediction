﻿using Application.Features.Commands;
using Application.Features.Queries;
using Application.Utils;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers.v1
{
    [ApiVersion("1.0")]
    public class UsersController : BaseController
    {
        public UsersController(IMediator mediator, IConfiguration configuration) : base(mediator, configuration)
        {
        }


        [HttpGet("byId")]
        public async Task<IActionResult> GetById([FromQuery] GetUserByIdQuery query)
        {
            if(query.Id == Guid.Empty)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("User not found because id is empty.", 404)));
            }
            var user = await mediator.Send(query);
            if (user == default)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("User not found.", 404)));
            }
            else
            {
                return Ok(user);
            }
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("byEmail")]
        public async Task<IActionResult> GetByEmail([FromQuery] GetUserByEmailQuery query)
        {
            if(string.IsNullOrEmpty(query.Email) || string.IsNullOrWhiteSpace(query.Email))
            {
                return BadRequest(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Email is required", 400)));
            }
            User user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = query.Email
            });
            if (user == default)
            {
                return NotFound(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("User not found.", 404)));
            }
            else
            {
                return Ok(user);
            }
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await mediator.Send(new GetUsersQuery()));
        }
        
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] string email)
        {
            return Ok(await mediator.Send(new DeleteUserByEmailCommand()
            {
                Email = email
            }));
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpPatch]
        public async Task<IActionResult> Update([FromHeader]string token, [FromBody] UpdateUserCommand command)
        {
            if(string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Token is not set", 401)));
            }
            string userEmail = AuthUtil.DecodeToken(token);
            if(string.IsNullOrEmpty(userEmail))
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }
            var user = await mediator.Send(new GetUserByEmailQuery()
            {
                Email = userEmail
            });
            if(user == null)
            {
                return Unauthorized(System.Text.Json.JsonSerializer.Serialize(new UnsuccessfulResponse("Invalid token", 401)));
            }
            command.Id = user.Id;
            await mediator.Send(command);

            return Ok(System.Text.Json.JsonSerializer.Serialize(new Response("User updated successfully!")));
        }

    }
}
