﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers.v1
{
    [Route("api/{version:apiVersion}/[controller]/")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected IMediator mediator;
        protected readonly IConfiguration _configuration;

        public BaseController(IMediator mediator, IConfiguration configuration)
        {
            this.mediator = mediator;
            _configuration = configuration;
        }

    }
}
