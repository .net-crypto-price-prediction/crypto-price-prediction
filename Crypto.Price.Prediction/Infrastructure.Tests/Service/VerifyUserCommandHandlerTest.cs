﻿using Application.Features.Commands;
using Application.Interfaces;
using Domain.Entities;
using FakeItEasy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests.Service
{
    public class VerifyUserCommandHandlerTest
    {
        private readonly VerifyUserCommandHandler handler;
        private readonly IUserRepository repository;

        public VerifyUserCommandHandlerTest()
        {
            this.repository = A.Fake<IUserRepository>();
            this.handler = new VerifyUserCommandHandler(this.repository);
        }

        [Fact]
        public async Task Given_CreateCoinHistoryCommandHandler_When_HandlerIsCalled_Then_AddAsyncCoinHistoryIsCalled()
        {
            await handler.Handle(new VerifyUserCommand(), default);
            A.CallTo(() => repository.GetByIdAsync(A<Guid>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => repository.UpdateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }
    }
}
