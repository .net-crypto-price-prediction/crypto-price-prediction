﻿using Application.Features.Commands;
using Application.Interfaces;
using Domain.Entities;
using FakeItEasy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests.Service
{
    public class UpdateUserCommandHandlerTest
    {
        private readonly UpdateUserCommandHandler handler;
        private readonly IUserRepository repository;

        public UpdateUserCommandHandlerTest()
        {
            this.repository = A.Fake<IUserRepository>();
            this.handler = new UpdateUserCommandHandler(this.repository);
        }
    }
}
 