﻿using Application.Features.Commands;
using Application.Interfaces;
using Domain.Entities;
using FakeItEasy;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests.Service
{
    public class CreateCoinHistoryCommandHandlerTest
    {
        private readonly CreateCoinHistoryCommandHandler handler;
        private readonly ICoinHistoryRepository repository;

        public CreateCoinHistoryCommandHandlerTest()
        {
            this.repository = A.Fake<ICoinHistoryRepository>();
            this.handler = new CreateCoinHistoryCommandHandler(this.repository);
        }

        [Fact]
        public async Task Given_CreateCoinHistoryCommandHandler_When_HandlerIsCalled_Then_AddAsyncCoinHistoryIsCalled()
        {
            await handler.Handle(new CreateCoinHistoryCommand(), default);
            A.CallTo(() => repository.AddAsync(A<CoinHistory>._)).MustHaveHappenedOnceExactly();
        }
    }
}
