﻿using Application.Features.Commands;
using Application.Interfaces;
using Domain.Entities;
using FakeItEasy;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests.Service
{
    public class CreateInterestCommandHandlerTest
    {
        private readonly CreateInterestCommandHandler handler;
        private readonly IInterestRepository repository;

        public CreateInterestCommandHandlerTest()
        {
            this.repository = A.Fake<IInterestRepository>();
            this.handler = new CreateInterestCommandHandler(this.repository);
        }

        [Fact]
        public async Task Given_CreateInterestCommandHandler_When_HandlerIsCalled_Then_AddAsyncInterestIsCalled()
        {
            await handler.Handle(new CreateInterestCommand(), default);
            A.CallTo(() => repository.AddAsync(A<Interest>._)).MustHaveHappenedOnceExactly();
        }
    }
}
