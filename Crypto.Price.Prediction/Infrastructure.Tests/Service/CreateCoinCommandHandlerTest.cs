﻿using Application.Features.Commands;
using Application.Interfaces;
using Domain.Entities;
using FakeItEasy;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.Tests.Service
{
    public class CreateCoinCommandHandlerTest
    {
        private readonly CreateCoinCommandHandler handler;
        private readonly ICoinRepository repository;
        
        public CreateCoinCommandHandlerTest()
        {
            this.repository = A.Fake<ICoinRepository>();
            this.handler = new CreateCoinCommandHandler(this.repository);
        }

        [Fact]
        public async Task Given_CreateCoinCommandHandler_When_HandlerIsCalled_Then_AddAsyncCoinIsCalled()
        {
            await handler.Handle(new CreateCoinCommand(), default);
            A.CallTo(() => repository.AddAsync(A<Coin>._)).MustHaveHappenedOnceExactly();
        }
    }
}
