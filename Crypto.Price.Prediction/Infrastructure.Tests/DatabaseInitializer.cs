﻿using Domain.Entities;
using Persistence.Context;
using System;
using System.Linq;

namespace Tests
{
    public class DatabaseInitializer
    {
        public static void Initialize(CryptoPricePredictionContext context)
        {
            if (context.Users.Any())
            {
                return;
            }
            Seed(context);
        }

        private static void Seed(CryptoPricePredictionContext context)
        {
            var users = new[]
            {
                new User
                {
                    FirstName = "Joe",
                    LastName = "Doe",
                    Email = "joe.doe@gmail.com",
                    Password = "password",
                    PreferredCurrency = "EUR"
                }
            };
            var coins = new[]
            {
                new Coin
                {
                    Symbol = "BTC",
                    Name = "Bitcoin",
                    Image = "image.jpg"
                }
            };
            var coinsHistory = new[]
            {
                new CoinHistory
                {
                    Symbol = "BTC",
                    Name = "Bitcoin",
                    Current_Price = 47000
                }
            };

            context.Users.AddRange(users);
            context.Coins.AddRange(coins);
            context.CoinsHistory.AddRange(coinsHistory);

            context.SaveChanges();
        }
    }
}
