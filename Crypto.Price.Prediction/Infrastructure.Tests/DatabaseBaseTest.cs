﻿using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using System;

namespace Tests
{
    public class DatabaseBaseTest : IDisposable
    {
        protected readonly CryptoPricePredictionContext context;
        public DatabaseBaseTest()
        {
            var options = new DbContextOptionsBuilder<CryptoPricePredictionContext>().UseInMemoryDatabase("TestDatabase").Options;
            context = new CryptoPricePredictionContext(options);
            context.Database.EnsureCreated();
            DatabaseInitializer.Initialize(context);
        }

        public void Dispose()
        {
            context.Database.EnsureDeleted();
            context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
