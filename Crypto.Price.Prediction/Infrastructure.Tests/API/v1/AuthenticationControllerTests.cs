﻿using Application.Features.Commands;
using Application.Features.Queries;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using WebAPI.Controllers.v1;
using Xunit;

namespace Tests
{
    public class AuthenticationControllerTests
    {
        private readonly AuthenticationController controller;
        private readonly IMediator mediator;
        private readonly IConfiguration configuration;

        public AuthenticationControllerTests()
        {
            mediator = A.Fake<IMediator>();
            configuration = A.Fake<IConfiguration>();
            controller = new AuthenticationController(mediator, configuration);
        }

        [Fact]
        public async void Given_AuthenticationController_When_CreateIsCalled_Then_ShouldCreateUser()
        {
            await controller.Create(new CreateUserCommand
            {
                FirstName = "Joe",
                LastName = "Doe",
                Email = "joe.doe@gmail.com",
                Password = "password",
                PreferredCurrency = "EUR"
            });
            A.CallTo(() => mediator.Send(A<GetUserByEmailQuery>._, default)).MustHaveHappenedTwiceExactly();
            A.CallTo(() => mediator.Send(A<CreateUserCommand>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_AuthenticationController_When_VerifyUserIsCalled_Then_ShouldVerifyUser()
        {
            await controller.VerifyUser(new Guid("77fdc8ff-2028-47bf-9e1a-9b6f59df4be8"));
            A.CallTo(() => mediator.Send(A<VerifyUserCommand>._, default)).MustHaveHappenedOnceExactly();
        }
    }
}
