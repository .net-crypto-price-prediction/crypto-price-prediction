﻿using Application.Features.Queries;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using WebAPI.Controllers.v1;
using Xunit;

namespace Tests
{
    public class UsersControllerTests
    {
        private readonly UsersController controller;
        private readonly IMediator mediator;
        private readonly IConfiguration configuration;

        public UsersControllerTests()
        {
            mediator = A.Fake<IMediator>();
            configuration = A.Fake<IConfiguration>();
            controller = new UsersController(mediator, configuration);
        }

        [Fact]
        public async void Given_UsersController_When_GetIsCalled_Then_ShouldReturnAn_UsersCollection()
        {
            await controller.Get();
            A.CallTo(() => mediator.Send(A<GetUsersQuery>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_UsersController_When_GetByEmailIsCalled_Then_ShouldReturnAn_User()
        {
            await controller.GetByEmail(new GetUserByEmailQuery
            {
                Email = "test@example.com"
            });
            A.CallTo(() => mediator.Send(A<GetUserByEmailQuery>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_UsersController_When_GetByIdIsCalled_Then_ShouldReturnAn_User()
        {
            await controller.GetById(new GetUserByIdQuery
            {
                Id = new Guid("80300f2b-cf25-4a8f-b55a-4dbb992cf5ef")
            });
            A.CallTo(() => mediator.Send(A<GetUserByIdQuery>._, default)).MustHaveHappenedOnceExactly();
        }
    }
}
