﻿using Application.Features.Queries;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using WebAPI.Controllers.v1;
using Xunit;

namespace Infrastructure.Tests.API.v1
{
    public class CoinsControllerTest
    {
        private readonly CoinsController controller;
        private readonly IMediator mediator;
        private readonly IConfiguration configuration;

        public CoinsControllerTest()
        {
            mediator = A.Fake<IMediator>();
            configuration = A.Fake<IConfiguration>();
            controller = new CoinsController(mediator, configuration);
        }

        [Fact]
        public async void Given_CoinsController_When_GetIsCalled_Then_ShouldReturnAListOfCoins()
        {
            await controller.Get();
            A.CallTo(() => mediator.Send(A<GetCoinsQuery>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_CoinsController_When_GetByIdIsCalled_Then_ShouldReturnACoin()
        {
            await controller.GetById(new GetCoinByIdQuery
            {
                Id = new Guid("77fdc8ff-2028-47bf-9e1a-9b6f59df4be8")
            });
            A.CallTo(() => mediator.Send(A<GetCoinByIdQuery>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_CoinsController_When_GetByNameIsCalled_Then_ShouldReturnACoin()
        {
            await controller.GetByName(new GetCoinByNameQuery
            {
                Name = "Bitcoin"
            });
            A.CallTo(() => mediator.Send(A<GetCoinByNameQuery>._, default)).MustHaveHappenedOnceExactly();
        }
    }
}
