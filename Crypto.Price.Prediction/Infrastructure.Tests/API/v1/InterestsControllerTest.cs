﻿using Application.Features.Queries;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using WebAPI.Controllers.v1;
using Xunit;

namespace Infrastructure.Tests.API.v1
{
    public class InterestsControllerTest
    {
        private readonly InterestsController controller;
        private readonly IMediator mediator;
        private readonly IConfiguration configuration;

        public InterestsControllerTest()
        {
            mediator = A.Fake<IMediator>();
            configuration = A.Fake<IConfiguration>();
            controller = new InterestsController(mediator, configuration);
        }

        [Fact]
        public async void Given_InterestsController_When_GetIsCalled_Then_ShouldReturnAn_InterestsCollection()
        {
            await controller.Get();
            A.CallTo(() => mediator.Send(A<GetAllInterestsQuery>._, default)).MustHaveHappenedOnceExactly();
        }
    }
}
