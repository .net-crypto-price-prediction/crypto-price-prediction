﻿using Application.Features.Queries;
using FakeItEasy;
using MediatR;
using Microsoft.Extensions.Configuration;
using WebAPI.Controllers.v1;
using Xunit;

namespace Infrastructure.Tests.API.v1
{
    public class CoinsHistoryControllerTest
    {
        private readonly CoinsHistoryController controller;
        private readonly IMediator mediator;
        private readonly IConfiguration configuration;

        public CoinsHistoryControllerTest()
        {
            mediator = A.Fake<IMediator>();
            configuration = A.Fake<IConfiguration>();
            controller = new CoinsHistoryController(mediator, configuration);
        }

        [Fact]
        public async void Given_CoinsHistoryController_When_GetIsCalled_Then_ShouldReturnA_CoinHistoryCollection()
        {
            await controller.Get();
            A.CallTo(() => mediator.Send(A<GetCoinHistoryQuery>._, default)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async void Given_CoinsHistoryController_When_GetByNameIsCalled_Then_ShouldReturnA_CoinHistory()
        {
            await controller.GetByName(new GetCoinHistoryByNameQuery
            {
                Name = "Bitcoin"
            });
            A.CallTo(() => mediator.Send(A<GetCoinHistoryByNameQuery>._, default)).MustHaveHappenedOnceExactly();
        }
    }
}
