﻿using Domain.Entities;
using FluentAssertions;
using Persistence.v1;
using System;
using Tests;
using Xunit;

namespace Infrastructure.Tests.Data
{
    public class InterestRepositoryTest : DatabaseBaseTest
    {
        private readonly InterestRepository repository;
        private readonly Guid userId;
        private readonly Guid coinId;

        public InterestRepositoryTest()
        {
            repository = new InterestRepository(context);
            UserRepository userRepository = new UserRepository(context);
            CoinRepository coinRepository = new CoinRepository(context);   
            userId = userRepository.GetByEmailAsync("joe.doe@gmail.com").Result.Id;
            coinId = coinRepository.GetByNameAsync("Bitcoin").Result.Id;
        }

        [Fact]
        public async void Given_UserId_WhenIdExists_Then_GetByUserIdAsyncShouldReturnAListOfInterest()
        {
            await repository.AddAsync(new Interest()
            {
                UserId = userId,
                CoinId = coinId,
            });

            var result = await repository.GetByUserIdAsync(userId);
            result[0].Should().BeOfType<Interest>();
        }

        [Fact]
        public async void Given_CoinAndUserId_WhenIdsExist_Then_GetByCoinAndUserIdAsyncShouldReturnAnInterest()
        {
            await repository.AddAsync(new Interest()
            {
                UserId = userId,
                CoinId = coinId,
            });

            var result = await repository.GetByCoinAndUserIdAsync(coinId, userId);
            result.Should().BeOfType<Interest>();
        }
    }
}
