﻿using Domain.Entities;
using FluentAssertions;
using Persistence.v1;
using Tests;
using Xunit;

namespace Infrastructure.Tests
{
    public class UserRepositoryTest : DatabaseBaseTest
    {
        private readonly UserRepository repository;
        private readonly string email;

        public UserRepositoryTest()
        {
            repository = new UserRepository(context);
            email = "joe.doe@gmail.com";
        }

        [Fact]
        public async void Given_UserEmail_WhenEmailExists_Then_GetByEmailAsyncShouldReturnAUser()
        {
            var result = await repository.GetByEmailAsync(email);
            result.Should().BeOfType<User>();
        }
    }
}
