﻿using Domain.Entities;
using FluentAssertions;
using Persistence.v1;
using Tests;
using Xunit;

namespace Infrastructure.Tests.Data
{
    public class CoinHistoryRepositoryTest : DatabaseBaseTest
    {
        private readonly CoinHistoryRepository repository;
        private readonly string name;

        public CoinHistoryRepositoryTest()
        {
            repository = new CoinHistoryRepository(context);
            name = "Bitcoin";
        }

        [Fact]
        public async void Given_CoinHistoryName_WhenNameExists_Then_GetByEmailAsyncShouldReturnAListOfCoinHistory()
        {
            var result = await repository.GetByName(name);
            result[0].Should().BeOfType<CoinHistory>();
        }
    }
}
