﻿using Domain.Entities;
using FluentAssertions;
using Persistence.v1;
using Tests;
using Xunit;

namespace Infrastructure.Tests.Data
{
    public class CoinRepositoryTest : DatabaseBaseTest
    {
        private readonly CoinRepository repository;
        private readonly string name;

        public CoinRepositoryTest()
        {
            repository = new CoinRepository(context);
            name = "Bitcoin";
        }

        [Fact]
        public async void Given_CoinName_WhenNameExists_Then_GetByNameAsyncShoudReturnACoin()
        {
            var result = await repository.GetByNameAsync(name);
            result.Should().BeOfType<Coin>();
        }
    }
}
