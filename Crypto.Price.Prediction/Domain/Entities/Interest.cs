﻿using Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Interest : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        
        [ForeignKey("Coin")]
        public Guid CoinId { get; set; }

        
    }
}
