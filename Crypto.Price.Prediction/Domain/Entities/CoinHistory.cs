﻿using Domain.Common;
using Microsoft.ML.Data;
using System.ComponentModel.DataAnnotations.Schema;
namespace Domain.Entities
{
    public class CoinHistory : BaseEntity
    {
        [ForeignKey("Coin")]
        public Guid CoinId { get; set; }
        public string? Symbol { get; set; }
        public string? Name { get; set; }
        public double? Current_Price { get; set; }
        public double? Market_Cap { get; set; }
        public double? Total_Volume { get; set; }
        public DateTime? Date { get; set; }
        public override string ToString()
        {
            return "Symbol: " + Symbol + ",  Name: " + Name + ", Current_Price: " + Current_Price + ", Market_Cap: " + Market_Cap
                + ", Total_Volume: " + Total_Volume + ", CoinId: " + CoinId + ", Date: " + Date.ToString();
        }
    }
}
