﻿using Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; } = null!;
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; } = null!;
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        public string Email { get; set; } = null!;
        [Required(ErrorMessage = "Password is required")]
        [MinLength(8)]
        public string Password { get; set; } = null!;
        public string PreferredCurrency { get; set; } = null!;
        public bool IsVerified { get; set; }
        public string? Token { get; set; }
    }
}
