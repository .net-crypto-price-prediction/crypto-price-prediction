﻿using Domain.Common;

namespace Domain.Entities
{
    public class Coin : BaseEntity
    {
        public string? Symbol { get; set; }
        public string? Name { get; set; }
        public string? Image { get; set; }
        public double? Ath { get; set; }
        public double? Ath_Change_Percentage { get; set; }
        public DateTime? Ath_Date { get; set; }
        public double? Atl { get; set; }
        public double? Atl_Change_Percentage { get; set; }
        public DateTime? Atl_Date { get; set; }
        public override string ToString()
        {
            return "Symbol: " + Symbol + ",  Name: " + Name + ",  ImageURL:" + Image + ",  ath: " + Ath;
        }

    }
}
